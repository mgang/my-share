# marp-share
## 简介
基于`marp`的markdown语法编写ppt文档。

## 目录内容说明
* it - 分享it相关的小经验、技巧等
* mind - 对一些问题的思考，记录部分关键点

## 部署
以`docker`容器方式运行，将文档放到`/data/marp-share`下即可。
``` shell
docker run -d --restart=always --name marp-share -v /data/marp-share:/home/marp/app -p 30088:8080 swr.cn-south-1.myhuaweicloud.com/mangoorg/marp-cli:latest -s .
```