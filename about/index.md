---
marp: true
paginate: true
header: '分享'
footer: '关于我们'
# theme: gaia
# class:
#   - gaia
style: |
    {
        font-size: 30px;
    }
---

# 香港之旅
## 行程
（充电宝，证件，衣服，google Map）
* 10点起床，做饭
* 12点出门，从车公庙方向去福田口岸（40分钟）
* 1点左右，通关前，要换港币，确认香港流量，地铁卡余额（支付宝二维码是否可用）


---
![](http://qn.meiflower.top/design/hotel.png)
