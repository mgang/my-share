---
marp: true
paginate: true
header: '分享'
footer: '基于docsify轻松构建您的在线笔记'
# theme: gaia
# class:
#   - gaia
style: |
    {
        font-size: 30px;
    }
---

## 基于docsify轻松构建您的在线笔记
* 缘起 + IDE环境
* 选型（hexo,hugo,vuepress,docsify等）
* 实现
  * docsify的入门示例
  * docsify的常用模板介绍  
  * docsify的常用插件说明
* 部署及实践
  * gitee pages部署
  * github pages部署
  * docker部署
* 回顾总结（为什么？怎么做？终于拥有）
            


---

## 缘起 + IDE环境
### 为什么写在线笔记
* 记笔记，是因为好记性不如烂笔头
* 在线化，是因为电子化+互联网化，随时随地可浏览，防丢失
### IDE 环境
* 编辑器 vs code
* 版本控制工具git + 代码托管gitee(或者github，无账号的话请注册)
* vs code git插件 + markdown的一些插件
  * markdown all in one
  * markdown preview enhanced

---
## 选型
* hexo
* hugo
* vuepress
* docsify（推荐）


---
# 实现
## docsify的入门示例
1. [docsify官方快速入门](https://docsify.js.org/#/zh-cn/quickstart?id=%e5%88%9d%e5%a7%8b%e5%8c%96%e9%a1%b9%e7%9b%ae)
2. 上传到gitee上

---
# 3-实现
## 3.2 docsify的模板介绍（自己整理，仅供参考）
1. [docsify模板仓库](https://gitee.com/mgang/my-template/tree/docsify/)
```
git clone -b docsify https://gitee.com/mgang/my-template.git
```
2. 结合[官方文档](https://docsify.js.org/#/zh-cn/)，介绍一下模板里的内容


---
## docsify的常用插件说明
### 字数统计
``` html
<!-- 字数插件 -->
<script src="https://cdn.jsdelivr.net/npm/docsify-count@latest/dist/countable.min.js"></script>
```
配置:
``` js
window.$docsify = {
  count:{
    countable:true,
    fontsize:'0.9em',
    color:'rgb(90,90,90)',
    language:'chinese'
  }
}
```
效果查看


---
## docsify的常用插件说明
### tabs选项卡

引入依赖文件
``` html
<script src="https://cdn.jsdelivr.net/npm/docsify-tabs@1"></script>
```
配置：
```
window.$docsify = {
  tabs: {
    persist    : true,      // default
    sync       : true,      // default
    theme      : 'classic', // default
    tabComments: true,      // default
    tabHeadings: true       // default
  }
}
```
---
* 例子演示
``` html
<!-- tabs:start -->
#### **English**
Hello!
#### **French**
Bonjour!
<!-- tabs:end -->
```
---
## docsify的常用插件说明
### alerts提示文字
``` html
<script src="https://unpkg.com/docsify-plugin-flexible-alerts"></script>
```
配置
``` js
window.$docsify = {
// note tip warning attention https://github.com/fzankl/docsify-plugin-flexible-alerts
  'flexible-alerts': {
    style: 'callout'
  },
}
```

---
例子：
```
> [!TIP]
> An alert of type 'tip' using global style 'callout'.

> [!NOTE]
> An alert of type 'note' using global style 'callout'.

> [!WARNING]
> An alert of type 'warning' using global style 'callout'.

> [!ATTENTION]
> An alert of type 'attention' using global style 'callout'.
```

---
## docsify的常用插件说明
### 最近更新记录
``` html
<script src="https://cdn.jsdelivr.net/npm/docsify-updated/src/time-updater.min.js"></script>
```
配置
``` js
window.$docsify = {
  timeUpdater: {
    text: "<div align='right' width='100%' style='color:gray;font-size:16px;margin-top:10px;'>最后更新时间: {docsify-updated}</div>",
    formatUpdated: "{YYYY}-{MM}-{DD} {HH}:{mm}",
  },
}
```
效果演示

---
## docsify的常用插件说明
### 进度条美化
``` html
<script src="https://cdn.jsdelivr.net/npm/docsify-progress@latest/dist/progress.min.js"></script>
```
配置
``` js
window.$docsify = {
  progress: {
    position: "top",
    color: "var(--theme-color,#42b983)",
    height: "3px",
  },
}
```
效果演示

---
## docsify的常用插件说明
### 回到顶部
``` html
<script src="https://cdn.jsdelivr.net/gh/Sumsung524/docsify-backTop/dist/docsify-backTop.min.js"></script>
```
配置
``` js
window.$docsify = {
  docsifyBackTop: {
    size: 32,           	// 数值，组件大小，默认值32。
    bottom: 15,         	// 数值，组件底部偏移距离，默认值15。
    right: 15,          	// 数值，组件右侧偏移距离，默认值15。
    logo: 'top',				// logo:字符串或svg矢量图代码，默认为svg代码图标。
    bgColor: '#42b983'    	// 背景颜色，#fff、pink等，logo为svg图标时，不填。
  }
}
```
效果演示


---
## docsify的常用插件说明
### 集成百度统计
``` js
<script>
  var _hmt = _hmt || [];
  (function() {
    var host = window.location.host;
    if(host.startsWith("localhost") || host.startsWith("127.0.0.1")){
      return ;
    }
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?17b94311795f1539081c4a0a0f14d4fb";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
  })();
</script>
```

---
## docsify的常用插件说明
### 集成plantuml
``` html
<script src="//unpkg.com/docsify-plantuml/dist/docsify-plantuml.min.js"></script>
```
配置
``` js
window.$docsify = {
  plantuml: {
    skin: 'default'
  },
}
```
演示（编写）

---
## markdown的任务列表写法
### 任务列表
```
* [x] 123
* [ ] xxx
```
效果查看

---
## docsify的常用插件说明
### 集成toc插件-docsify-toc
显示文章右侧目录
```
<!-- toc -->
<link rel="stylesheet" href="https://unpkg.com/docsify-toc@1.0.0/dist/toc.css">
<script src="https://unpkg.com/docsify-toc@1.0.0/dist/toc.js"></script>
js配置：
toc: {
  scope: '.markdown-section',
  headings: 'h1, h2, h3, h4, h5, h6',
  title: '目录',
},
```
效果查看

---
## docsify的常用插件说明
### 集成docsify-pugin-toc插件
显示文章右侧目录
```
<link rel="stylesheet" href="https://unpkg.com/docsify-plugin-toc@1.3.1/dist/light.css">
<script src="https://unpkg.com/docsify-plugin-toc@1.3.1/dist/docsify-plugin-toc.min.js"></script>
js配置：
toc: {
  tocMaxLevel: 5,
  target: 'h2, h3, h4, h5, h6',
  ignoreHeaders:  ['<!-- {docsify-ignore} -->', '<!-- {docsify-ignore-all} -->']
},
```
效果查看

---
## docsify的常用插件说明
### 集成gitalk评论
> 参考：https://zhuanlan.zhihu.com/p/260781932
> https://docsify.js.org/#/zh-cn/plugins?id=gitalk
1. index.html引入js，css资源
``` html
<!-- GitTalk评论 -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/gitalk/dist/gitalk.css">
<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/gitalk.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/gitalk/dist/gitalk.min.js"></script>
```
2. 在index.html内添加div容器
``` html
<div id="gitalk-container"></div>
```

---
3. 先申请 [GitHub Application](https://link.zhihu.com/?target=https%3A//github.com/settings/applications/new)，保存id和secret
4. index.html中通过js来渲染
``` js
const gitalk = new Gitalk({
  clientID: '085dd85605dcd3f627e6', //github app id和secret
  clientSecret: '81a557cf600cf8946b79c080f80072af5e9aa447',
  repo: 'docsify-note-01',// 存放评论的仓库
  owner: 'mg0324',
  admin: ['mg0324'],
  distractionFreeMode: false,
  id:decodeURI(window.location.pathname)
});
gitalk.render('gitalk-container')
```
通过docsify-note演示，集成gitalk。


---
## docsify的常用插件说明
### 集成docsify-accordion插件(手风琴)
多用在faq，问答场景。
```
<!-- docsify-accordion -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify-accordion/src/style.css">
<script src="//cdn.jsdelivr.net/npm/docsify-accordion/src/index.js"></script>
```
效果在基础demo内查看

---
## docsify的常用插件说明
### 集成docsify-demo插件(html preview)
html预览，可查看源码
```
<!-- html preview demo-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify-demo@latest/dist/index.min.css"/>
<script src="//cdn.jsdelivr.net/npm/docsify-demo@latest/dist/index.min.js"></script>
```
效果在基础demo内查看

---
## docsify的常用插件说明
### 集成docsify-pangu插件(加空格)
在英文和中文之间，自动添加空格。（强迫症）
注意可能会和其他插件有兼容问题。
```
<!-- docsify-pangu 加空格-->
<script src="//cdn.jsdelivr.net/npm/docsify-pangu/lib/pangu.min.js"></script>
```
开启并演示效果

---
## docsify的常用插件说明
### 集成docsify-remote-markdown插件
嵌入远程md文件
```
<!-- remote-markdown -->
<script src="//cdn.jsdelivr.net/npm/docsify-remote-markdown/dist/docsify-remote-markdown.min.js"></script>
配置：
// 开启远程md
remoteMarkdown: {
  tag: 'rmd',
},
```
在高级示例页内演示效果

---
## docsify的常用插件说明
集成docsify-drawio插件
显示drawio图形，在示例页内演示效果。也可以把drawio的png引入md。
```
<script src="https://cdn.jsdelivr.net/npm/docsify-drawio/viewer.min.js"></script>
<script src='https://cdn.jsdelivr.net/npm/docsify-drawio/drawio.js'></script>
配置：
markdown: {
  renderer: {
    code: function (code, lang) {
      if (lang === 'drawio') {
        if (window.drawioConverter) {
          console.log('drawio 转化中')
          return window.drawioConverter(code)
        } else {
          return `<div class='drawio-code'>${code}</div>`
        }
      } else {
        return this.origin.code.apply(this, arguments);
      }
    }
  }
},
```

---
## docsify的常用插件说明
### 集成docsify-kroki插件
从文字描述创建图表！
[docsify-kroki](https://www.npmjs.com/package/docsify-kroki) [kroki](https://kroki.io/) - Kroki 提供统一的 API，支持 BlockDiag（BlockDiag、SeqDiag、ActDiag、NwDiag、PacketDiag、RackDiag）、BPMN、Bytefield、C4（使用 PlantUML）、D2、DBML、Ditaa、Erd、Excalidraw、GraphViz、Mermaid、Nomnoml、Pikchr 、PlantUML、Structurizr、SvgBob、UMLet、Vega、Vega-Lite、WaveDrom……还有更多！

```
<!-- docsify-kroki 支持全热门图形化语言-->
<script src="//cdn.jsdelivr.net/npm/docsify-kroki"></script>
```
demo演示

---
## docsify的常用插件说明
### 内置搜索说明
``` 
<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/search.min.js"></script>
// 默认配置：
search: 'auto',
```
详细配置：[跳转](https://docsify.js.org/#/zh-cn/plugins?id=%e5%85%a8%e6%96%87%e6%90%9c%e7%b4%a2-search)。

docsify-note 演示

---
## docsify的常用插件说明
### 常用插件说明 docsify-contributors
```
<script src="//gcore.jsdelivr.net/npm/docsify-contributors@latest/dist/index.min.js"></script>
```
配置：
```
contributors: {
    repo: 'mg0324/docsify-note',
    ignores: [],
    style: {
        color: '#ffffff',
        bgColor: '#404040'
    },image: {
        size: 30,
        isRound: true,
        margin: '0.5em'
    },load: {
        isOpen: true,
        color: "#009999"
    }
  }
```
docsify-note 演示

---
## docsify的常用插件说明
###  docsify-to-pdf
为了将在线笔记导出为pdf文档
[docsify-to-pdf配置](https://www.npmjs.com/package/docsify-to-pdf)
docsify-note 演示


---
## docsify的常用插件说明
###  docsify-pdf-embed-plugin
为了内嵌显示pdf
[docsify-pdf-embed-plugin配置](https://www.npmjs.com/package/docsify-pdf-embed-plugin)
docsify-note 演示


---
## docsify的进阶使用
### vue之书签示例
[示例页](https://mg0324.github.io/docsify-note/#/bookmark/README)

---
## docsify的进阶使用
### vue使用说明
[官方说明](https://docsify.js.org/#/zh-cn/vue)

示例演示


---
## docsify补充说明
### 构建更新日志
1. 新建文件`CHANGELOG.md`,用来记录更新日志。
2. 在`_navbar.md`中加上更新日志的说明。
3. 养成维护更新日志的习惯，这样在线笔记能稳定维护。（基于版本，或者时间都行）

经验演示

---
## docsify补充说明
### docsify-note的示例说明
基础示例
高级示例

演示

---
## docsify补充说明
### docsify-note的示例说明
基础示例
高级示例

演示

---
## docsify补充说明
### 代码高亮支持

[代码高亮](https://docsify.js.org/#/zh-cn/language-highlight)


Java代码高亮演示

---
## docsify补充说明
### cdn的说明

* bootcdn - 稳定、快速、免费的前端开源项目 CDN 加速服务，支持国内。
* jsdelivr - 用于开源项目的免费 CDN，针对来自 npm 和 GitHub 的 JS 和 ESM 交付进行了优化。适用于所有网络格式。国内外都支持。（推荐使用）
* cdnjs - cdnjs 是一项免费的开源 CDN 服务，受到超过 12.5% 的所有网站的信任，每月服务超过 2000 亿个请求，由 Cloudflare 提供支持。国内外都支持。
* unpkg - unpkg 是一个快速的全球内容交付网络，适用于 npm 上的所有内容。国内外都支持。

建议把index.html里的资源路径更换为jsdelivr的。

---
## docsify补充说明
### cdn的说明2
（4月9号更新，最近发现`cdn.jsdelivr.net`被墙无法访问，建议切换到`gcore.jsdelivr.net`上）
![bg right w:600](cdn.png)

---
## docsify补充说明
### cdn的说明3
*（4月9号更新，最近发现`cdn.jsdelivr.net`被墙无法访问，建议切换到`gcore.jsdelivr.net`上）
* 4月20多号，小伙伴反馈说`gcore`的404了，但是`cdn`上的又可以了。
* 自建资源
  * 将需要用到的资源下载到本地，以静态资源方式部署
  * 将需要用到的资源上传到云oss上，比如七牛云oss



---
## docsify补充说明
### emoji 表情
[官方emoji说明](https://docsify.js.org/#/zh-cn/plugins?id=emoji)

演示

---
## docsify补充说明
### 自动生成sidebar

[docsify g](https://cli.docsifyjs.org/#/?id=generate-command)

* 生成_sidebar.md文件
* 也可以自己手动编写。




---
## docsify自定义插件
需要了解并熟悉
1. [docsify插件列表](https://docsify.js.org/#/zh-cn/plugins)
2. [docsify插件开发示例](https://docsify.js.org/#/zh-cn/write-a-plugin)
3. [npm发布自己的包](https://www.jianshu.com/p/32a59d8eb0f0)
4. [我的npm的packages](https://www.npmjs.com/settings/mgang/packages)
5. [docsify-plugin-toc插件参考](https://github.com/justintien/docsify-plugin-toc)

后续视频：（已开发完成如下插件）- 并发布共享到给[官方](https://docsify.js.org/#/zh-cn/awesome) 
1. [docsify-baidu-tj](https://www.npmjs.com/package/docsify-baidu-tj) - 百度统计
2. [docsify-gitalk](https://www.npmjs.com/package/docsify-gitalk) - gitalk评论
3. [docsify-giscus](https://www.npmjs.com/package/docsify-giscus) - giscus评论
4. [docsify-busuanzi](https://www.npmjs.com/package/docsify-busuanzi) - busuanzi显示网站访问量

---
## docsify自定义插件
### docsify-baidu-tj 百度统计
不使用插件前，直接在index.html里script集成。（不优雅，不美观）
使用后：
[docsify-baidu-tj](https://www.npmjs.com/package/docsify-baidu-tj) - 百度统计

---
## docsify自定义插件
### docsify-gitalk 评论
不使用插件前，直接在index.html里script集成。（不优雅，不美观）
使用后：
[docsify-gitalk](https://www.npmjs.com/package/docsify-gitalk) - gitalk评论


---
## docsify自定义插件
### docsify-giscus 评论
不使用插件前，直接在index.html里script集成。（不优雅，不美观）
使用后：
[docsify-giscus](https://www.npmjs.com/package/docsify-giscus) - giscus评论


---
## docsify自定义插件
### docsify-mango-valine 评论

使用后：
[docsify-mango-valine](https://www.npmjs.com/package/docsify-mango-valine) - valine评论

* docsify-valine - 参数没暴露
* docsify-waline - 需要部署vercel，对小白不友好
* docsify-gitalk - 需github登录才能评论
* docsify-giscus - 需github登录才能评论
* docsify-mango-valine - 匿名评论


---
## docsify自定义插件
### docsify-busuanzi 显示网站访问量
[不蒜子](https://busuanzi.ibruce.info/) - 两行代码 搞定计数
[docsify-busuanzi](https://www.npmjs.com/package/docsify-busuanzi) - 网站计数器

---
## docsify自定义插件
### docsify-ads 广告推广
当您的在线笔记有了一些流量后，可以做一下推广
[docsify-ads](https://www.npmjs.com/package/docsify-ads) - 广告推广

源码说明
演示

--- 
## docsify的常用插件说明
* 图床使用七牛云oss(免费)

---
## docsify的常用插件说明
* 滑入图片后懒加载并显示   

---
# 部署及实践
## gitee pages部署
> 已录制
* 在编辑好在线笔记一个版本后，可以发布
* 在gitee上的仓库需要是公开的，且账号通过了实名认证
* 开启gitee pages服务后，即可公网访问

https://mgang.gitee.io/docsify-note/

演示

---
# 部署及实践
## github pages部署
* 在编辑好在线笔记一个版本后，可以发布
* 将在线笔记内容上传到github上对应仓库，`.nojekyll`文件防止下划线开头文件被忽略。
* 开启github pages服务，在push内容后，会自动运行流水线触发更新。（基于action)

https://mg0324.github.io/docsify-note/

演示


---
# 部署及实践
## docker容器部署
* 维护好在线笔记版本，等待发布
* 将笔记上传到远程服务器上的文件夹下，如`/data/docsify-note`
* 运行`sujaykumarh/docsify serve`容器，[docsify镜像](https://hub.docker.com/r/sujaykumarh/docsify)
``` shell
docker run --name docsify-note -d -p 30000:3000 -v /data/docsify-note:/docs sujaykumarh/docsify serve
```

http://hw.meiflower.top:30000/

演示

---
# 部署及实践
## 公有云paas平台railway部署
> railway每个月有5美元和500小时的额度。
* 在编辑好在线笔记一个版本后，可以发布
* 将在线笔记内容上传到github上对应仓库，`.nojekyll`文件防止下划线开头文件被忽略。
* 通过railway平台部署自己的在线笔记。


演示

---
# 部署及实践
## 公有云paas平台netlify部署
* 在编辑好在线笔记一个版本后，可以发布
* 通过netlify平台部署自己的在线笔记。


演示

---
# mangodoc篇
* 介绍（已处理）
* 1.0.4版本说明（已处理）
* 1.3.0版本
* [favicon](https://favicon.io/) 给网站设置小图标

---
# docsify-note微信交流群

经过一段时间的更新分享后，有一些朋友在使用过程中遇到了一些问题（私信，评论），所以建立一个微信群来分享交流docsify知识，更好地帮助使用者（小白，非程序员），欢迎大家加群！


欢迎加我微信，备注（docsify-note交流），我会拉您进群！！！

![bg right](https://mg.meiflower.top/oss/docsify-note/mango-wx.jpeg)