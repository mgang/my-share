---
marp: true
paginate: true
header: '分享'
footer: '实用经验'
# theme: gaia
# class:
#   - gaia
style: |
    {
        font-size: 30px;
    }
---


## 1. 闲置旧电脑变身高配云服务器
* b站视频地址：https://www.bilibili.com/video/BV1Y841157YF/
* 痛点
  * 低配云服务器过期，续费价格昂贵
    * 双11以新用户购买
* 树莓派或者闲置旧电脑(4C8G)
  * linux开启sshd
  * windows开启远程桌面 - https://blog.csdn.net/mg0324?spm=1019.2139.3001.5343
* 通过frp内网穿透，变身云服务器
  * 需要公网IP


---

## 2. 各种操作系统尝鲜
* b站视频地址：https://www.bilibili.com/video/BV1Lv4y1o7Ps/
---
![bg](./02/deepin.png)

---
![bg](./02/manjaro.png)

---
![bg](./02/ubuntu.png)

---
![bg](./02/macos.png)

---
![bg](./02/win11.png)

---

## 3. 构建自己的精简专属网盘
![bg right](./03/disk.png)
* b站视频地址：https://www.bilibili.com/video/BV1f44y1S7AW/
* kiftd: https://kohgylw.gitee.io/ 目前正在使用的精简网盘程序，一款专门面向个人、团队和小型组织的私有网盘系统。无论是在家庭、学校还是在办公室，您都能立刻开始使用它！（界面不够美观）

---

* 开源选型2
  * zfile: https://www.zfile.vip/ 基于 Java 的在线网盘程序，支持对接 S3、OneDrive、SharePoint、又拍云、本地存储、FTP、SFTP 等存储源，支持在线浏览图片、播放音视频，文本文件、Office、obj（3d）等文件类型。能齐全，支持各种存储（本地，云oss)
  * nextcloud: https://nextcloud.com/ 企业级云盘，支持各种平台，web、android和ios等。
* 基于开源项目kiftd构建：https://kohgylw.gitee.io/ 开源 个人 小团队
* 利用docker运行，磁盘挂载，有条件可以自己购买云硬盘
  * alpine-kiftd镜像：https://hub.docker.com/r/mangomei/kiftd
* 专属网盘演示：http://disk.meiflower.top

---

## 4. 今日事今日毕的todolist
* b站视频地址：https://www.bilibili.com/video/BV1q44y1m7VG/
* 基于开源项目flask-todolist构建： https://github.com/rtzll/flask-todolist.git python flask sqlite 精简的待办列表
* 界面文字微调，中文化
* 打包为docker镜像，部署到华为云服务器
* 配置nginx域名代理：http://todo.meiflower.top
* flask-todolist的简单使用


---

## 5. Electron桌面化Web应用

* electron 艾那窗
* 基于开源项目electron构建：https://www.electronjs.org/zh/ 将您的web应用打包为桌面应用（内嵌浏览器，访问web地址）
* 演示：桌面化 http://todo.meiflower.top

![bg right](05/todo.png)

---

## 6. 搞定你域名的https证书
* b站视频地址：https://www.bilibili.com/video/BV1S8411G7VS/
* 博客地址：https://mg.meiflower.top/mb/post/handleway/%E5%9F%9F%E5%90%8D%E8%AF%81%E4%B9%A6%E6%9B%B4%E6%96%B0/
* 域名：meiflower.top，可以自行购买。域名备案：需要在自己的云服务器厂商里走备案流程（比如在腾讯云购买的，就在腾讯云上走流程，且需要按主页备案规范挂网页，不然会被检查要求整改）
* 代理服务器nginx部署，利用docker或者传统方式部署
* 免费的dns：https://dns.he.net/index.cgi
* 免费证书申请：https://freessl.cn/  原来可以申请免费的1年的证书，现在只能3个月了。
  
![bg left:25% 200%](06/www_https.png)

---

## 7. 让chatGPT成为你学习的好帮手

* 前提：注册chatGPT，参考博客：https://blog.csdn.net/duoshehuan6005/article/details/128184450
* 想一个主题或者问题，然后一直深入探索下去，直到自己豁然开朗
* 实践

---


## 8. 再次进入世界最大的电子图书馆Z-Library
***因为内容不允许发布***
* z-library前段时间从互联网下线，目前在tor网络可用。
  * 互联网地址：https://singlelogin.me/
  * tor地址：bookszlibb74ugqojhzhg2a63w5i2atv5bqarulgczawnbmsb6s6qead.onion
* 可以自己购买vpn，或者其他方式，比如科学上网
  * 本人使用的是西游VPN，欢迎大家使用该链接 https://xiyou4you.us/r/?s=20224329
* 下载自己的tor浏览器，然后登录即可。
* 或者安卓应用来访问z-library，这个需要手机端使用的。


---

## 8.1 快来啊！Z-B-A-L-I-B可以访问了
> 没目标：https://www.bilibili.com/video/BV18R4y1m7VN/
* 请使用洋大葱浏览器带云访问洋大葱地址: zzz:bookszlibb74ugqojhzhg2a63w5i2atv5bqarulgczawnbmsb6s6qead.onion:zzz

---

## 9. vs code里的4款实用插件
> https://www.bilibili.com/video/BV16d4y1E7T4/
* [marp](https://marp.app/) - 利用markdown来编写ppt，可导出为html、pptx和pdf
* [drawio](https://github.com/jgraph/drawio) - 画图工具，画完图后可直接引入到md中
* [markmap](https://markmap.js.org/) - 利用markdown来编写思维导图
* [plantUML](https://plantuml.com/zh/) - 画图工具，支持uml标准和一些其他的图形


---
## 10. 拥有自己的网页版的vs code
需更新

---
## 11. 将chatgpt机器人装入到微信
* [wechat-chatgpt](https://github.com/fuergaosi233/wechat-chatgpt)
* 演示
* 部署